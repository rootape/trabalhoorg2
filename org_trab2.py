import struct
import sys
from shutil import copyfile
import time

inicio = time.time()

if len(sys.argv) != 2:
    b = 10000
else:
    b = int(sys.argv[1])

registroCEP = struct.Struct("72s72s72s72s2s8s2s")
cepColumn = 5

itera = int(699307/b)
#print "Itera = %d" %itera

listn = []

for i in range(0, int(itera)):
    listn.append(b)
if (699307 % b) != 0:
    itera += 1
    listn.append(699307 % b)
    
#print "Itera = %d" %itera
#print "Geral = %d" %len(listn)
lis = []

f = open("cep.dat", "r")

line = f.read(registroCEP.size)

contador = 0
count = 0

#Gera Lis
while contador != 699307:
    r = registroCEP.unpack(line)
    a = [r[cepColumn], contador * 300]
    lis.append(a)
    line = f.read(registroCEP.size)
    contador += 1

f = open("cep.dat", "r")
t = open("cep_ord.dat", "w")

c = 0

while c != itera:
    #print c
    lista = []

    for i in range(0, listn[c]):
        lista.append(lis[(c*b) + i])
   
    lista.sort()

    for j in range(0, listn[c]):
        f.seek((lista[j])[1], 0)
        line = f.read(registroCEP.size)
        t.write(line)

    c += 1       
        
fim = time.time()

print (fim - inicio)
print "Terminado"

f.close
t.close


